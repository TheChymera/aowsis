\input{slides/header.tex}

\title[Standards-Compliant Repositing of Small Animal MRI Data — Video Summary of \href{https://doi.org/10.3389/fninf.2020.00005}{\texttt{[doi:10.3389/fninf.2020.00005]}}]{Standards-Compliant Repositing of Small Animal MRI Data}
\subtitle{
	Video Summary of:\\
	\href{https://doi.org/10.3389/fninf.2020.00005}{\footnotesize\texttt{[ doi:10.3389/fninf.2020.00005 ]}}\\
	\vspace{.5em}
	Source code:\\
	\href{https://bitbucket.org/TheChymera/aowsis}{\footnotesize\texttt{[ bitbucket.org/TheChymera/aowsis ]}}
	}
\author[Horea Christian]{Horea Christian\\\href{https://twitter.com/TheChymera}{\footnotesize\texttt{[ @TheChymera ]}}\vspace{-.5em}}
\institute{Institute for Biomedical Engineering, ETH and University of Zurich}
\begin{document}
	\begin{frame}
		\titlepage
	\end{frame}
	\section{Introduction}
		\subsection{Key concerns}
			\begin{frame}{Raw Data Recourse}
				\begin{itemize}
					\item Increases processing transparency and reproducibility.
					\item Prevents undocumented “fixes”, such as:
					\begin{itemize}
						\item \textit{ex post facto} manipulations.
						\item outlier filtering.
					\end{itemize}
					\item Required for data integration (e.g. multi-center studies) and data reuse.
				\end{itemize}
			\end{frame}
			\begin{frame}{Provenance}
				\begin{itemize}
					\item The rawest data is the best recourse, but Processing scope constrains the choice.
				\end{itemize}
				\vspace{-1.9em}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/data}
					\vspace{-1.5em}
					\caption{Provenance flowchart with data states on disk represented as folders (article fig. 1)}
				\end{figure}
				\vspace{-1.2em}
				\begin{itemize}
					\item Vendor formats (e.g. Bruker ParaVision) are not good standards.
					\item Automatic conversion to a standard format (red arrow edge) would:
					\begin{itemize}
						\item permit automatic creation of a standardized 3D “raw” data recourse.
						\item make vendor format raw data archives implicitly standardizable.
					\end{itemize}
				\end{itemize}
			\end{frame}
			\begin{frame}{Formats}
				\begin{itemize}
					\item Vendor formats are intransparent and commonly not open for contributions.
					\item Database formats are monolithic and often require complicated specialized technologies.
					\item BIDS \cite{bids}:
					\begin{itemize}
						\item builds on the widely-used NIfTI \cite{nifti} standard.
						\item stores metadata as human-readable text accessible to GNU utilities such as \textcolor{mg}{\texttt{diff}} \cite{diff}.
						\item uses a GUI and CLI accessible directory structure.
					\end{itemize}
				\end{itemize}
				\vspace{-1.9em}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/bids}
					\vspace{-1.5em}
					\caption{BIDS can integrate volumetric and metadata throughout processing and analysis (article fig. 2a)}
				\end{figure}
				\vspace{-1.2em}
			\end{frame}
	\section{Workflow}
		\subsection{Features and overview}
			\begin{frame}{Repositing Strucure}
				\begin{itemize}
					\item Bruker Paravision directory structure is automatically resolved
					\item Corresponding text files are automatically queried for BIDS fields
				\end{itemize}
				\begin{figure}
					\centering
					\includegraphics[width=.99\textwidth]{img/files}
					\vspace{-.2em}
					\caption{Bruker Paravision to BIDS correspondence and field provenance (article fig. 2b)}
				\end{figure}
			\end{frame}
			\begin{frame}[fragile]{Function Interface}
				\begin{figure}
					\centering
					\begin{subfigure}{.4\textwidth}
\begin{shaded}
\vspace{.4em}
\begin{Verbatim}[fontsize=\footnotesize,commandchars=\\\{\}]
\textcolor{mygreen}{user@host} \textcolor{blue!50}{~ $} SAMRI bru2bids\bslash
  -o /var/tmp/samri_testing/bash\bslash
  -f '\opencurl"acquisition":["EPI"]\closecurl'\bslash
  -s '\opencurl"acquisition":["TurboRARE"]\closecurl'\bslash
  /usr/share/samri_bindata
\end{Verbatim}
\end{shaded}
						\label{fig:bash}
					\end{subfigure}\hspace{1.7em}
					\begin{subfigure}{.52\textwidth}
						\vspace{-.8em}
\begin{shaded}
\begin{Verbatim}[fontsize=\footnotesize,commandchars=\\\{\}]
from samri.pipelines.reposit import bru2bids
bru2bids(\textcolor{red}{'/usr/share/samri_bindata/'},
  functional_match=\opencurl\textcolor{red}{'acquisition'}:[\textcolor{red}{'EPI'}]\closecurl,
  structural_match=\opencurl\textcolor{red}{'acquisition'}:[\textcolor{red}{'TurboRARE'}]\closecurl,
  out_base=\textcolor{red}{'/var/tmp/samri_testing/pytest/'},
  )
\end{Verbatim}
\vspace{-.5em}
\end{shaded}
					\end{subfigure}
				\caption{Bash and Python interfaces, made synchronously available via the Argh package (article fig. 3)}
				\end{figure}
				\begin{itemize}
					\item Highly parameterized, with general-purpose defaults.
					\item Supports scan selection via BIDS key-value dictionaries for each “datatype”.
				\end{itemize}
			\end{frame}
			\begin{frame}{Operator Guidelines}
				\begin{figure}
					\begin{subfigure}{.64\textwidth}
						\centering
						\includegraphics[width=\textwidth]{img/ss.png}
					\end{subfigure}\hfill
					\begin{subfigure}{.338\textwidth}
						\centering
						\includegraphics[width=\textwidth]{img/at.png}
					\end{subfigure}
					\caption{Interpretable and correct metadata needs to be entered in the operator input fields (article fig. S2)}
				\end{figure}
			\end{frame}
			\begin{frame}[fragile]{Preexisting Data Compliance}
				\begin{figure}
					\centering
					\begin{subfigure}{.6\textwidth}
\begin{shaded}
\vspace{0.5em}
\begin{Verbatim}[fontsize=\footnotesize,commandchars=\\\{\}]
\textcolor{cyan}{@@ -3225 +3225 @@}
\textcolor{red}{-<displayName>MSME_200um (E6)</displayName>}
\textcolor{mygreen}{+<displayName>acq-MSME200um_T2w (E6)</displayName>}
\textcolor{cyan}{@@ -4218 +4218 @@}
\textcolor{red}{-<displayName>GE_EPI_sat_mat96_1mm (E7)</displayName>}
\textcolor{mygreen}{+<displayName>acq-geEPI_task-rest_bold (E7)</displayName>}
\end{Verbatim}
\vspace{0.3em}
\end{shaded}
						\label{fig:bash}
					\end{subfigure}\hspace{1.7em}
					\begin{subfigure}{.32\textwidth}
\begin{shaded}
\vspace{-.3em}
\begin{Verbatim}[fontsize=\footnotesize,commandchars=\\\{\}]
\textcolor{cyan}{@@ -10,2 +10,2 @@}
 ##$SUBJECT_id=( 60 )
\textcolor{red}{-<MD1704-Mc285AB-P02>}
\textcolor{green}{+<Mc285AB>}
\textcolor{cyan}{@@ -28,2 +28,2 @@}
 ##$SUBJECT_study_name=( 64 )
\textcolor{red}{-<MD1704_RsLemurs_Rs02>}
\textcolor{green}{+<R02>}
\end{Verbatim}
\vspace{-.7em}
\end{shaded}
						\label{fig:python}
					\end{subfigure}
				\caption{Malformed data records can be redressed by editing the \textcolor{mg}{\texttt{Scanprogram.scanProgram}} and \textcolor{mg}{\texttt{subject}} files, respectively (article fig. 4)}
				\end{figure}
			\end{frame}
	\section{Results}
		\subsection{Demonstrating the features}
			\begin{frame}[fragile]{Example Data Archive}
				\begin{itemize}
					\item Automatically installable on Gentoo Linux via the Science Overlay \cite{sci}:
						\begin{minipage}{0.91\textwidth}
						\vspace{0.2em}
							\begin{shaded}
								\vspace{-0.2em}
\begin{Verbatim}[fontsize=\footnotesize,commandchars=\\\{\}]
\textcolor{red}{root@host} \textcolor{blue!50}{~ $} emerge samri_bindata
\end{Verbatim}
								\vspace{-0.5em}
							\end{shaded}
						\end{minipage}
					\item Downloadable and manually installable via the command line:
						\begin{minipage}{0.91\textwidth}
						\vspace{0.5em}
							\begin{shaded}
								\vspace{-0.2em}
\begin{Verbatim}[fontsize=\footnotesize,commandchars=\\\{\}]
\textcolor{red}{root@host} \textcolor{blue!50}{~ $} wget http://chymera.eu/distfiles/samri_bindata-0.4.tar.xz
\textcolor{red}{root@host} \textcolor{blue!50}{~ $} tar xvf samri_bindata-0.4.tar.xz
\textcolor{red}{root@host} \textcolor{blue!50}{~ $} mv samri_bindata-0.4 /usr/share/samri_bindata
\end{Verbatim}
								\vspace{-0.5em}
							\end{shaded}
						\end{minipage}
				\end{itemize}
			\end{frame}
			\begin{frame}[fragile]{Mouse Data Repositing}
				\begin{figure}
					\centering
					\begin{minipage}{0.35\textwidth}
						\begin{shaded}
							\vspace{-0.2em}
\begin{Verbatim}[fontsize=\tiny,commandchars=\\\{\}]
\textcolor{myblue}{\textbf{/usr/share/samri_bidsdata}}
├── \textcolor{myblue}{\textbf{20151208_182500_4007_1_4}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
├── \textcolor{myblue}{\textbf{20170317_203312_5691_1_5}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
├── \textcolor{myblue}{\textbf{20171024\_165248\_MD1704\_Mc285AB_P02\_1\_1}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
├── \textcolor{myblue}{\textbf{20171106_184345\_21\_1\_1}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
└── \textcolor{myblue}{\textbf{20180529_041333_SN9879_1_1}}
    ├── [...]
    ├── ScanProgram.scanProgram
    └── subject
\end{Verbatim}
							\vspace{-0.8em}
						\end{shaded}
					\end{minipage}\hspace{2em}%
					\begin{minipage}{0.54\textwidth}
						\vspace{0.3em}
						\begin{minipage}{\textwidth}
							\begin{shaded}
								\vspace{-0.5em}
\begin{Verbatim}[fontsize=\tiny,commandchars=\\\{\}]
\textcolor{mygreen}{user@host} \textcolor{blue!50}{~ $} SAMRI bru2bids\bslash
	-o /var/tmp/samri_testing/bash/mouse\bslash
	-f '\opencurl"acquisition":["EPI"]\closecurl'\bslash
	-s '\opencurl"acquisition":["TurboRARE"]\closecurl'\bslash
	/usr/share/samri_bindata
\end{Verbatim}
								\vspace{-0.7em}
							\end{shaded}
						\end{minipage}
						\vspace{.2em}

						\begin{minipage}{\textwidth}
							\begin{shaded}
\begin{Verbatim}[fontsize=\tiny,commandchars=\\\{\}]
\textcolor{myblue}{\textbf{/var/tmp/samri_testing/bash/mouse}}
└─ \textcolor{myblue}{\textbf{bids}}
   ├─ dataset_description.json
   └─ \textcolor{myblue}{\textbf{sub-5691}}
      ├─ \textcolor{myblue}{\textbf{ses-ofMpF}}
      │  ├─ \textcolor{myblue}{\textbf{anat}}
      │  │  ├─ sub-5691_ses-ofMpF_acq-TurboRARE_T2w.json
      │  │  └─ sub-5691_ses-ofMpF_acq-TurboRARE_T2w.nii.gz
      │  └─ \textcolor{myblue}{\textbf{func}}
      │     ├─ sub-5691_ses-ofMpF_task-CogB_acq-EPI_run-0_bold.json
      │     ├─ sub-5691_ses-ofMpF_task-CogB_acq-EPI_run-0_bold.nii.gz
      │     └─ sub-5691_ses-ofMpF_task-CogB_acq-EPI_run-0_events.tsv
      └─ sub-5691_sessions.tsv
\end{Verbatim}
							\end{shaded}
						\end{minipage}
						\vspace{.2em}
					\end{minipage}
					\vspace{-.3em}
					\caption{
						Selection according to scan type (article fig. 5)
					}
				\end{figure}
			\end{frame}
			\begin{frame}[fragile]{Rat Data Repositing}
				\begin{figure}
					\centering
					\begin{minipage}{0.35\textwidth}
						\begin{shaded}
							\vspace{-0.2em}
\begin{Verbatim}[fontsize=\tiny,commandchars=\\\{\}]
\textcolor{myblue}{\textbf{/usr/share/samri_bidsdata}}
├── \textcolor{myblue}{\textbf{20151208_182500_4007_1_4}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
├── \textcolor{myblue}{\textbf{20170317_203312_5691_1_5}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
├── \textcolor{myblue}{\textbf{20171024\_165248\_MD1704\_Mc285AB_P02\_1\_1}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
├── \textcolor{myblue}{\textbf{20171106_184345\_21\_1\_1}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
└── \textcolor{myblue}{\textbf{20180529_041333_SN9879_1_1}}
    ├── [...]
    ├── ScanProgram.scanProgram
    └── subject
\end{Verbatim}
							\vspace{-0.8em}
						\end{shaded}
					\end{minipage}\hspace{2em}%
					\begin{minipage}{0.54\textwidth}
						\vspace{0.3em}
						\begin{minipage}{\textwidth}
							\begin{shaded}
								\vspace{-0.1em}
\begin{Verbatim}[fontsize=\tiny,commandchars=\\\{\}]
\textcolor{mygreen}{user@host} \textcolor{blue!50}{~ $} SAMRI bru2bids\bslash
	-o /var/tmp/samri_testing/bash/rat\bslash
	-f '\opencurl"acquisition":["seEPI"]\closecurl'\bslash
	/usr/share/samri_bindata
\end{Verbatim}
								\vspace{-0.4em}
							\end{shaded}
						\end{minipage}
						\vspace{.2em}

						\begin{minipage}{\textwidth}
							\begin{shaded}
\begin{Verbatim}[fontsize=\tiny,commandchars=\\\{\}]
\textcolor{myblue}{\textbf{/var/tmp/samri_testing/bash/rat/}}
└─ \textcolor{myblue}{\textbf{bids}}
   ├─ dataset_description.json
   └─ \textcolor{myblue}{\textbf{sub-21}}
      ├─ \textcolor{myblue}{\textbf{ses-noFUSr0}}
      │  └─ \textcolor{myblue}{\textbf{func}}
      │     ├─ sub-21_ses-noFUSr0_task-MhBu_acq-seEPI_run-1_bold.json
      │     ├─ sub-21_ses-noFUSr0_task-MhBu_acq-seEPI_run-1_bold.nii.gz
      │     ├─ sub-21_ses-noFUSr0_task-MhBu_acq-seEPI_run-1_events.tsv
      │     ├─ sub-21_ses-noFUSr0_task-rest_acq-seEPI_run-0_bold.json
      │     ├─ sub-21_ses-noFUSr0_task-rest_acq-seEPI_run-0_bold.nii.gz
      │     └─ sub-21_ses-noFUSr0_task-rest_acq-seEPI_run-0_events.tsv
      └─ sub-21_sessions.tsv
\end{Verbatim}
							\end{shaded}
						\end{minipage}
						\vspace{.2em}
					\end{minipage}
					\vspace{-.3em}
					\caption{
						Selection according to scan type (article fig. 5)
					}
				\end{figure}
			\end{frame}
			\begin{frame}[fragile]{Lemur Data Repositing}
				\begin{figure}
					\centering
					\begin{minipage}{0.35\textwidth}
						\begin{shaded}
							\vspace{-0.2em}
\begin{Verbatim}[fontsize=\tiny,commandchars=\\\{\}]
\textcolor{myblue}{\textbf{/usr/share/samri_bidsdata}}
├── \textcolor{myblue}{\textbf{20151208_182500_4007_1_4}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
├── \textcolor{myblue}{\textbf{20170317_203312_5691_1_5}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
├── \textcolor{myblue}{\textbf{20171024\_165248\_MD1704\_Mc285AB_P02\_1\_1}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
├── \textcolor{myblue}{\textbf{20171106_184345\_21\_1\_1}}
│   ├── [...]
│   ├── ScanProgram.scanProgram
│   └── subject
└── \textcolor{myblue}{\textbf{20180529_041333_SN9879_1_1}}
    ├── [...]
    ├── ScanProgram.scanProgram
    └── subject
\end{Verbatim}
							\vspace{-0.8em}
						\end{shaded}
					\end{minipage}\hspace{2em}%
					\begin{minipage}{0.54\textwidth}
						\vspace{0.3em}
						\begin{minipage}{\textwidth}
							\begin{shaded}
								\vspace{-0.1em}
\begin{Verbatim}[fontsize=\tiny,commandchars=\\\{\}]
\textcolor{mygreen}{user@host} \textcolor{blue!50}{~ $} SAMRI bru2bids\bslash
	-o /var/tmp/samri_testing/bash/lemur\bslash
	-f '\opencurl"acquisition":["geEPI"]\closecurl'\bslash
	-s '\opencurl"acquisition":["MSME200um"]\closecurl'\bslash
	/usr/share/samri_bindata
\end{Verbatim}
								\vspace{-0.4em}
							\end{shaded}
						\end{minipage}
						\vspace{.2em}

						\begin{minipage}{\textwidth}
							\begin{shaded}
							\vspace{-.2em}
\begin{Verbatim}[fontsize=\tiny,commandchars=\\\{\}]
\textcolor{myblue}{\textbf{/var/tmp/samri_testing/bash/lemur/}}
└─ \textcolor{myblue}{\textbf{bids}}
   ├─ dataset_description.json
   └─ \textcolor{myblue}{\textbf{sub-Mc285AB}}
      ├─ \textcolor{myblue}{\textbf{ses-R02}}
      │  ├─ \textcolor{myblue}{\textbf{anat}}
      │  │  ├─ sub-Mc285AB_ses-R02_acq-MSME200um_T2w.json
      │  │  └─ sub-Mc285AB_ses-R02_acq-MSME200um_T2w.nii.gz
      │  └─ \textcolor{myblue}{\textbf{func}}
      │     ├─ sub-Mc285AB_ses-R02_task-rest_acq-geEPI_run-0_bold.json
      │     ├─ sub-Mc285AB_ses-R02_task-rest_acq-geEPI_run-0_bold.nii.gz
      │     └─ sub-Mc285AB_ses-R02_task-rest_acq-geEPI_run-0_events.tsv
      └─ sub-Mc285AB_sessions.tsv
\end{Verbatim}
							\vspace{-.5em}
							\end{shaded}
						\end{minipage}
						\vspace{.2em}
					\end{minipage}
					\vspace{-.3em}
					\caption{
						Selection according to scan type (article fig. 5)
					}
				\end{figure}
			\end{frame}
	\section{Implementation}
		\subsection{How the sausage gets made}
			\begin{frame}{Technologies}
				\begin{itemize}
					\item Bruker ParaVison data parsing via custom regex matching in SAMRI \cite{samri}
					\item Stimulation protocol file integration via SAMRI support for COSplay \cite{cosplay}
					\item \textcolor{mg}{\texttt{2dseq}} conversion to NIfTI via Bru2Nii \cite{bru2nii}
					\item Data tracking via Pandas \cite{pandas}
					\item Workflow management via nipype \cite{nipype}
				\end{itemize}
			\end{frame}
			\begin{frame}{Workflow Structure}
				\begin{itemize}
					\item Iteration of the same workflow type over subject-session combinations
					\item Per-datatype iteration of dedicated workflows
				\end{itemize}
				\begin{figure}
					\begin{subfigure}{.49\textwidth}
						\centering
						\includedot[width=1.1\textwidth]{data/graph_structural}
					\end{subfigure}\hfill
					\begin{subfigure}{.49\textwidth}
						\centering
						\includedot[width=1.1\textwidth]{data/graph_functional}
					\end{subfigure}
					\vspace{-2em}
					\caption{
						Workflow layouts for structural and functional scans, respectively (article fig. 6)
						}
				\end{figure}
			\end{frame}
	\section{Discussion}
		\subsection{So what?}
			\begin{frame}{Now you can:}
				\begin{itemize}
					\item easily produce standardized BIDS archives for:
						\begin{itemize}
							\item manual inspection
							\item distribution
							\item accessible collaboration
						\end{itemize}
					\item archive Bruker ParaVision data as your ultimate recourse, confident that it will be automatically usable as needed by downstream processing.
					\item chain this workflow with your pre/processing for raw-data-to-analysis-output automation.
				\end{itemize}
			\end{frame}
	%\section{References}
		\tiny
		% We use biblatex here, see `slides/header.tex`
		%\bibliographystyle{unsrt}
		%\bibliography{./bib}
		\printbibliography
\end{document}
