\section{The Workflow}

\begin{figure*}[h!]
	\centering
	\begin{subfigure}{.97\textwidth}
		\vspace{-2.6em}
		\centering
		\includedot[width=\textwidth]{data/bids}
		\vspace{-3em}
		\caption{
			Processing flowchart, breaking down the Bruker to BIDS repositing step (highlighted in \cref{fig:raw}) to depict data-metadata integration, and downstream metadata encoding in the BIDS directory hierarchy and corresponding metadata files.
			}
		\label{fig:bids}
	\end{subfigure}
	\begin{subfigure}{.97\textwidth}
		\vspace{2em}
		\centering
		\includegraphics[width=.95\textwidth]{img/files}
		\vspace{.75em}
		\caption{
			Directory tree overview of Bruker to BIDS repositing.
			Depicted are source and result directories, with arrows indicating which files the most relevant BIDS variable fields are sourced from.
			Date information is coded in cyan, and is sourced directly from the ParaVision scan directory name.
			The conversion presented herein shows the mouse data form the test data collection, and is performed given the instructions in \cref{fig:bash}: note that no BIDS entries from the 4007 and SN9879 subject directories were processed (as well as from any of the other species).
			This is happening by design and because no entries in any of the other scans match the selection criteria.
			}
		\label{fig:files}
	\end{subfigure}
	\caption{
		\textbf{The Paravision-to-BIDS repositing process automatically interprets the source data structure and determines corresponding variables in the BIDS standard.}
		Depicted are color-coded overviews of data and metadata streams during the SAMRI \textcolor{mg}{\texttt{bru2bids}} repositing process, with
			the data matrix content coded in gray,
			the subject field in red,
			the session field in green,
			the task field in blue,
			the acquisition field in ochre,
			and the modality suffix in purple.
		}
	\label{fig:bru2bids}
\end{figure*}

The workflow, entitled \textcolor{mg}{\texttt{bru2bids}} (Bruker ParaVision to BIDS), is distributed as part of SAMRI \cite{samri}, a free and open source workflow package of the ETH and University of Zurich Institute for Biomedical Engineering.
This repositing workflow can be used stand-alone, but also serves as a gateway to all the further workflows included in the SAMRI package (encompassing dedicated solutions for all analysis steps showcased in \cref{fig:raw}).
As such, the ParaVision-to-BIDS workflow not only permits users to convert data into a format which is more widely supported and flexible, but also easily links to reference implementations for BIDS-based small animal processing functionalities (e.g. registration \cite{prep}).

The workflow reposits data and metadata from the ParaVision standard into a BIDS-compliant form, notifying the user of BIDS validation possibilities upon completion.
The repositing process automatically handles the conversion of data from ParaVision volumetric reconstruction files (\textcolor{mg}{\texttt{2dseq}}) to NIfTI files.
Additionally, it assigns metadata from the specific ParaVision text files to either the NIfTI header, the BIDS metadata files, or the BIDS directory hierarchy, as applicable.
A simplified overview of this process is presented in \cref{fig:bids}.
A more extensive break-down of metadata sourcing --- showing the actual input and output files, and highlighting metadata fields represented in the data paths --- is laid out in \cref{fig:files}.

The repositing functionality described herein can be accessed from both Bash and Python, via \textcolor{mg}{\texttt{SAMRI bru2bids}} or \textcolor{mg}{\texttt{samri.pipelines.reposit.bru2bids()}}, respectively.
Invocation variants are illustrated in \cref{fig:cli}, and link to the same code implementation.
The \textcolor{mg}{\texttt{bru2bids}} function is highly parameterized, with the same parameter set available in either Bash or Python.
A full list of parameters can be obtained by executing the \textcolor{mg}{\texttt{SAMRI bru2bids --help}} command from the console. % Three hyphens to output two, because life is hard.
The current parameter listing for Bash is presented under \cref{fig:params}.

Notable parameters include the functional, diffusion-weighted, and structural scan specification.
These three selectors each use an input dictionary (pairs of one \textit{key}, which is a BIDS metadata string, and a list of accepted \textit{values}), to identify which scans are to be reposited.
Examples of such dictionaries are given in \cref{fig:cli}, where scans with an “EPI” acquisition field are categorized as functional, and files with a “TurboRARE” acquisition field are categorized as structural.
The repositing pipeline is run sequentially for all scan categories, as separate processes are required for each scan type (examples of the internal processing nodes are shown in \cref{fig:nipype}).
Further parameters not included in the workflow to date, such as magnetic resonance spectroscopy (MRS) data selection, can easily be implemented, given the availability and familiarity with enough example data, by copying and editing the process instructions from present parameters inside the \textcolor{mg}{\texttt{samri.pipelines.reposit.bru2bids()}} function.

\begin{figure*}[h!]
	\vspace{1em}
	\centering
	\begin{subfigure}{.47\textwidth}
\begin{shaded}
\begin{Verbatim}[fontsize=\footnotesize,commandchars=\\\{\}]

\textcolor{mygreen}{user@host} \textcolor{blue!50}{~ $} SAMRI bru2bids\bslash
	-o /var/tmp/samri_testing/bash\bslash
	-f '\opencurl"acquisition":["EPI"]\closecurl'\bslash
	-s '\opencurl"acquisition":["TurboRARE"]\closecurl'\bslash
	/usr/share/samri_bindata
\end{Verbatim}
\end{shaded}
		\caption{
			Bash invocation, repositing data into a BIDS directory located under \textcolor{mg}{\texttt{/var/tmp/samri\_testing/bash}}.
			}
		\label{fig:bash}
	\end{subfigure}\hfill
	\begin{subfigure}{.47\textwidth}
\begin{shaded}
\begin{Verbatim}[fontsize=\footnotesize,commandchars=\\\{\}]
from samri.pipelines.reposit import bru2bids
bru2bids(\textcolor{red}{'/usr/share/samri_bindata/'},
	inflated_size=\textcolor{cyan}{False},
	functional_match=\opencurl\textcolor{red}{'acquisition'}:[\textcolor{red}{'EPI'}]\closecurl,
	structural_match=\opencurl\textcolor{red}{'acquisition'}:[\textcolor{red}{'TurboRARE'}]\closecurl,
	out_base=\textcolor{red}{'/var/tmp/samri_testing/pytest/'},
	)
\end{Verbatim}
\vspace{-1.4em}
\end{shaded}
		\caption{
			Python invocation, repositing data into a BIDS directory located under \textcolor{mg}{\texttt{/var/tmp/samri\_testing/pytest}}.
			}
		\label{fig:python}
	\end{subfigure}
	\caption{
		\textbf{Both Bash and Python can be used to access the repositing functionality.}
		As the Bash binding is auto-generated from the Python function, features become available synchronously, and inspection can be coherently performed regardless of the invocation language.
		Both code snippets specify the exact same instructions regarding the data source: they indicate that ParaVision standard data from \textcolor{mg}{\texttt{/usr/share/samri\_bindata/}} is to be reposited into a BIDS standard form, categorizing scans with an “EPI” acquisition string as functional, and files with a “TurboRARE” acquisition string as structural.
		Both of the above invocations are included in the package test suite.
		}
	\label{fig:cli}
\end{figure*}
