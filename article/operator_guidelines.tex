\subsection{Operator Guidelines}

The process of scan categorization and metadata sourcing for BIDS conversion is contingent on the presence of operator input records interpretable by the workflow.
As the ParaVision metadata files contain free-field input, adherence to a minimal set of guidelines is necessary to ensure unambiguous error-free conversion.

The acquisition of data in the Bruker ParaVision graphical user interface is commenced by creating a new study in the “Study Registration” window (\cref{fig:gui}).
In this window the operator should fill in the “Animal ID” entry corresponding to the intended BIDS subject identifier (i.e. the \textcolor{mg}{\texttt{sub}} field in the resulting path and file names),
and the “Study Name” entry corresponding to the intended BIDS session identifier (i.e. the \textcolor{mg}{\texttt{ses}} field in the resulting path and file names).
Notably, the values for both of these fields should be BIDS-compliant --- meaning that they should contain only alphanumeric characters, necessarily excluding underscores and hyphens, which are used in the BIDS standard as field separators.

Once the study is created, scans should be renamed in the graphical user interface (before or after acquisition) to contain the additional relevant metadata information according to the BIDS standard.
%Currently supported fields are acquisition, trial, and the modality suffix.
Thus, a resting-state scan acquired with an EPI sequence resolving BOLD contrast should contain the following string in the “Instruction Name” column of the ParaVision interface: \textcolor{mg}{\texttt{acq-seEPI\_task-rest\_bold}}.
The format is composed out of the BIDS short identifier (e.g. \textcolor{mg}{\texttt{acq}} for acquisition), followed by a hyphen and the desired metadata field value (e.g. \textcolor{mg}{\texttt{seEPI}} for spin-echo echo-planar imaging).
The level of detail in these fields is at the discretion of the operator, as per the flexible nature of the standard.
If recognizing the EPI variant at-a-glance is deemed irrelevant for the data at hand, this field may simply be assigned a value of \textcolor{mg}{\texttt{EPI}}, or could conversely be expanded to include an arbitrary amount of additional detail.
Pairs of BIDS short identifiers and desired values should be separated by underscores, with the modality suffix appended at the end after a final underscore separator.

Additional BIDS fields such as the run ordinal number (e.g. \textcolor{mg}{\texttt{run-0}}, as seen in \cref{fig:files}), are automatically determined by the \textcolor{mg}{\texttt{bru2bids}} workflow.
The modality suffix, if not explicitly specified by the operator, can also be automatically assigned in a small number of cases where it is unambiguous (e.g. a scan with a FLASH acquisition but with no operator defined modality will be assigned a \textcolor{mg}{\texttt{T1w}} modality suffix, and similarly, one with a TruboRARE acquisition will be assigned a \textcolor{mg}{\texttt{T2w}} suffix).

Entering metadata in this fashion during scanner operation creates records which can directly serve as input for analysis workflows, and are thus immediately ready for sharing or analysis.
Examples for such interpretable metadata scans are available in the SAMRI ParaVision testing data archive, and the example strings specified in this section are sourced from the rat data identified in the archive by the \textcolor{mg}{\texttt{20171106\_184345\_21\_1\_1}} directory name.
