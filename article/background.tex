\section{Introduction}

\begin{figure*}[h!]
	\vspace{-2.6em}
	\centering
	\includedot[width=\textwidth]{data/data}
	\vspace{-3em}
	\caption{
		Data provenance flowchart, with the leftmost data being the rawest.
		Folder nodes represent data states on disk, with nodes suitable as raw data recourse highlighted with the word raw in parentheses.
		The introduction of a standardized conversion process (red arrow edge) would permit the creation of a 3D format representation usable as raw data recourse, as well as the sharing of Bruker acquisition system volumetric reconstruction data (also highlighted in red).
		}
	\label{fig:raw}
\end{figure*}

Magnetic resonance imaging (MRI), and functional MRI (fMRI) are highly popular methods in the field of neuroscience.
Their high tissue penetration makes them eminently suited for reporting features at the whole-brain level \textit{in vivo}.
High assay coverage is particularly relevant for an organ as holistic in its function as the brain, as it facilitates the interrogation of not only sensitivity but also regional specificity.
However, MRI methods generate signal via nuclear spin polarisation --- which is commonly very weak --- and characteristically posess low intrinsic sensitivity.
Additionally, fMRI methods rely on highly indirect measures of neuronal activity, and are consequently susceptible to numerous confounding factors.

In animal fMRI in particular, subject preparation, and more specifically cerebrovascular parameters \cite{Schroeter2016} and anesthesia \cite{Schlegel2015, Bukhari2018} are widely known drivers of result variability.
In order to integrate data which may be thus strongly confounded --- as well as in order to clarify the confounds themselves \cite{Grandjean2019} --- it is vital that data is shared in a raw state, i.e. having undergone no or as little processing as possible.
Raw data sharing increases transparency and reproducibility, as data can be assumed to be free from undocumented “fixes”.
Such attempts at \textit{ex post facto} data improvement may not just include data matrix manipulations, but also outlier (subject or session) filtering.
While valid rationales for both outlier filtering and data editing exist, these processes are best performed in a transparent and well-documented fashion, leaving the raw data untouched as an ultimate recourse.

As published data is intended for reuse, it is reasonable to assume that it may be employed to explore hypotheses other than those under the constraints of which it was originally acquired.
In such cases, it is vital that the shared entry and feature pool be as inclusive as possible.
It is likely, above all in the effort of methodological comparison and improvement, that what is an artefact or outlier for the interrogation of a narrow hypothesis, may constitute a strong driver of the effect of another hypothesis.
Therefore, it is the best choice for small animal MRI researchers to publish data in as raw a form as possible.

The documentation of directed information processing is known as data provenance, and in the effort of establishing a point of recourse it is helpful to map out data traces to the earliest record or earliest record in digital form.
In \cref{fig:raw}, a simplified summary of data provenance is showcased, based on the most common features of small animal MRI.
While the rawest data is in theoretical terms the best possible recourse, the extent of this overview illustrates that the choice of a raw data origin point is also constrained by the scope of a researcher's work.
Particularly the first step, reconstructing volumetric information from the time domain k-space record, is commonly not covered by modern MRI pipelines, and instead left to the original acquisition software.

Whether directly from the k-space file or from the reconstructed image, the data needs to be converted into a standard, vendor-independent form.
Standards are a cornerstone of scientific collaboration, as they concomitantly enable result comparability and data integration.
They can, however, also be potentially restrictive, since a standard may impose artificial limitations on features or artificial requirements not pertinent to a particular study.
Such limitations may materialize in negative restrictions on software tools (e.g. proprietary standards may preclude data processing with non-proprietary tools), restrictions on hypotheses (e.g. data organized by one hierarchical principle may lead to loss or obfuscation of certain category correspondence relationships), or positive restrictions of technologies (e.g. data available in highly specific formats might require usage of and familiarity with highly specialized tools) --- with the latter restriction being particularly relevant in database standards.

In order to access the benefits and mitigate the pitfalls of standards compliance, data should be migrated to a form which is openly and thoroughly documented, and easily accessible for user manipulation.
In the field of small animal MRI, the vast majority acquisition devices (\SI{\sim 80}{\percent}) are produced by the Bruker Corporation, and thus the largest segment of data is initially formatted according to the ParaVision standard.
This standard is largely transparent, with most metadata stored in plain-text files.
The data, however, are stored in a binary format, which strongly diverges from the \textit{de facto} standard of NIfTI \cite{nifti}, and for which extensive documentation is not openly available.
Conversion tools from the ParaVision standard to NIfTI exist \cite{bru2nii,bruker2nifti}, and have more recently also been made available from the manufacturer (presently only in closed-source form and only in ParaVision 360, without backward compatibility).
Contingent on the scope limitations of the NIfTI format itself, these tools can however not repackage the majority of metadata represented in ParaVision standard plain-text files.
This situation exemplifies how the utility of standards is not only contingent on their suitability for use as a common origin format, but also on their flexibility to accommodate all relevant information.

The Brain Imaging Data Structure (BIDS) standard \cite{bids}, is a prominent candidate for repositing small animal magnetic resonance imaging data.
It is thoroughly tested and well adopted in the field of human MRI, and its extensible and permissive nature makes it easily adaptable to small animal data ---
as well as generally accommodating for broad swathes of eclectic use cases.
The standard builds upon the NIfTI format \cite{nifti}, one of the most widely used formats for high-level neuroimaging analysis, which is compact, as it offers on-the-fly lossless compression, and general-purpose, as its header only contains the minimal amount of metadata required for spatiotemportal image representation.
In addition, BIDS offers an extendable specification for metadata, stored in sidecar text files.
This separation of minimal and full metadata space makes BIDS easily and incrementally accessible for new users and portable to further modalities, as the core requirements can rapidly be met and further relevant metadata fields can be processed and added as they become required for analysis.
The standard's usage of plain-text metadata files also makes them accessible to ubiquitous, minimal, and Free and Open Source (FOSS) tools, e.g. the Standard GNU Utilities, or equivalent core utility implementations.
Particularly, the representation of metadata as text allows data set versions with increments in metadata availability to be compared via \textcolor{mg}{\texttt{diff}}-type commands \cite{diff}, which is less feasible for binary data.
The format is organized around a simple directory hierarchy, with key metadata fields captured in the file and path names.
This makes BIDS data intuitive to access from both a console and a graphical user interface.

Given varying specifications, it is common for standards to not map fully onto each other (e.g. one metadata field may not have a clear correspondence relationship in both standards).
As data conversion is always based only on the most recent parent format, this means that the risk of data (or more specifically, metadata) loss or obfuscation grows with each transition to a new standard.
Thus, in the example of \cref{fig:raw}, collaborative potential is best served if both the “3D Format” representation is infused with sufficient and sufficiently accessible metadata, \textit{and} the original “Volumetric Reconstruction” is rendered shareable.

Both these goals can be attained by the introduction of an automated open-source workflow which can perform the standard transition.
As such, all metadata fields which are identified as equivalent between the ParaVision and BIDS standards can be made accessible in the final form.
Conversely, if ParaVision standard data is automatically interpretable as input for a concatenation of processing workflows, this original form can also serve as a shareable raw data recourse.

%These methods are, however, reliant on atoms with very high concentrations (commonly hydrogen) to generate signal, and are thus not directly sensitive to either membrane potentials or proteomics.
%In order to establish a relationship between MR signal and these biologically highly meaningful descriptors of the brain, MRI methods are often rendered sensitive to endo- or exogenous contrast agents.
%Most prominent of these contrast agents is Hemoglobin, of which the oxygenated form is paramagnetic and the deoxygenated form is diamagnetic, and which is delivered in greater quantity to brain regions in more metabolically intensive states.
%Due to the reliance on such complex

