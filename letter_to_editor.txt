The collaborative processing, aggregation, and verification of data is contingent on common and openly documented formats.
Most small animal MRI data is acquired in the Bruker ParaVision format, which could thus be seen as more-or-less common in the sub-field.
This format is, however, not openly documented, and not compatible with higher-level data processing tools, most of which are developed in the far larger field of human MRI.
To address this problem, we present a pipeline which converts Burker ParaVision data into the openly documented and developed BIDS standard, which is also compatible with a large number of higher-level tools.
In order to make this conversion fully automated, we present a set of operator guidelines, as well as a quick but complete instruction set for how to render preexisting data accessible to the converter.
We exemplify these processes using data collections from mice, rats, and lemurs, which we openly distribute in a testing archive for our tools.
We also discuss how this first repositing step fits into the larger data provenance picture, and that it allows recourse to data which is more raw, and thus more free of undocumented editing operations.

We believe that a flexible, common, and open standard for *raw* data can vastly enhance downstream collaboration in the field --- as it did among our direct collaborators.
