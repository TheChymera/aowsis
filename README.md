# An Automated Open-Source Workflow for Standards-Compliant Integration of Small Animal Magnetic Resonance Imaging Data

These are the content files used to generate scientific communication materials for the project originally titled “An Automated Open-Source Workflow for Standards-Compliant Integration of Small Animal Magnetic Resonance Imaging Data”.

## Compilation Instructions

The document can be compiled with the following command:

```
./compile.sh
```
